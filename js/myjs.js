//Href for register button
$("#registerButton").click(function() {
  location.href= "registration.php";
});

var title = document.title;

if (title == "Homepage") {
  $("#myNavbar").find($("a")).removeClass("active");
  $("#myNavbar").find($("#homePageLink")).addClass("active");
} else if (title == "Google Maps Test") {
  $("#myNavbar").find($("a")).removeClass("active");
  $("#myNavbar").find($("#googleMapsTestLink")).addClass("active");
}
