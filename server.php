<?php
session_start();

//Variables
$username = "";
$email = "";
$errors = array();

//Connect to the database
$db = mysqli_connect('localhost', 'root', '', 'mysql_derust');

//Register user
if (isset($_POST['reg_user'])) {
  if (isset($_SESSION['username'])) {
    header('location: index.php');
    exit;
  } else {
    //Receive all input values from form
    $username = mysqli_real_escape_string($db, $_POST['username']);
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $password_1 = mysqli_real_escape_string($db, $_POST['password1']);
    $password_2 = mysqli_real_escape_string($db, $_POST['password2']);

    //Make sure form is filled in properly
    if (empty($username)) { array_push($errors, "Username is required!"); }
    if (empty($email)) { array_push($errors, "Email is required!"); }
    if (empty($password_1)) { array_push($errors, "Password is required!"); }
    if ($password_1 != $password_2) { array_push($errors, "The two passwords do not match!"); }

    //Make sure the username & email are available in the database
    $user_check_query = "SELECT * FROM users WHERE username='$username' or email='$email' LIMIT 1";
    $result = mysqli_query($db, $user_check_query);
    $user = mysqli_fetch_assoc($result);
    //The actual availability check
    if ($user) {
      if ($user['username'] === $username) {
        array_push($errors, 'Username is unavailable!');
      }
      if ($user['email'] === $email) {
        array_push($errors, "Email is unavailable!");
      }
    }

    //Register if there are no errors
    if (count($errors) == 0) {
      $password = password_hash($password_1, PASSWORD_DEFAULT);

      $query = "INSERT INTO users (username, email, password) VALUES ('$username', '$email', '$password')";
      mysqli_query($db, $query);
      $_SESSION['username'] = $username;
      $_SESSION['succes'] = "You are now logged in!";
      header('location: index.php');
    }
  }
}

//Login user
if (isset($_POST['login_user'])) {
  if (isset($_SESSION['username'])) {
    header('location: index.php');
    exit;
  } else {
    //Check if user and password were filled in
    if (empty(trim($_POST['username']))) {
      array_push($errors, "Username is required!");
    } else if (empty(trim($_POST['password']))) {
      array_push($errors, "Password is required!");
    } else {
      $username = trim($_POST['username']);
      $password = trim($_POST['password']);
    }

    //Validate credentials
    if (count($errors) == 0) {
      $sql = "SELECT username, password FROM users WHERE username = ?";

      if ($stmt = mysqli_prepare($db, $sql)) {
        mysqli_stmt_bind_param($stmt, "s", $param_username);

        //Set parameters
        $param_username = $username;

        //Execute prepared statement
        if (mysqli_stmt_execute($stmt)) {
          //Store result
          mysqli_stmt_store_result($stmt);
          //Check if username exists, if yes then verify password
          if (mysqli_stmt_num_rows($stmt) == 1) {
            //Bind result variables
            mysqli_stmt_bind_result($stmt, $username, $hashed_password);
            if (mysqli_stmt_fetch($stmt)) {
              if (password_verify($password, $hashed_password)) {

                //Check if passwords needs rehashing
                if (password_needs_rehash($hashed_password, PASSWORD_DEFAULT)) {
                  $new_hashed_password = password_hash($password, PASSWORD_DEFAULT);

                  $sql = "UPDATE users SET password='$new_hashed_password' WHERE username='$username'";
                }

                //Password is correct
                $_SESSION['username'] = $username;
                $_SESSION['succes'] = "You are now logged in!";

                //Reload page
                header('location: index.php');
              } else {
                //Password incorrect
                array_push($errors, "Password is incorrect!");
              }
            }
          } else {
            //Username incorrect
            array_push($errors, "Username is incorrect!");
          }
        }
      } else {
        //idk man
        array_push($errors, "Oops! Something went wrong!");
      }
    }
    //Fuck my shit up
    mysqli_stmt_close($stmt);
  }
}

//Logout user
if (isset($_POST['logout_user'])) {
  session_destroy();
  header('location: index.php');
}

?>
