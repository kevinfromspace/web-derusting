<?php
  if (isset($_SESSION['username'])) {
    header("index.php");
  }
  $title = "Register";
  include("headernavbar.php");
?>

<?php
if (!isset($_SESSION['username'])) {
  include("errors.php");
}
?>

<!-- Put page specific HTML here -->
<form method="post" id="registerForm">
  <div class="aRegisterInput">
    <div class="form-group">
      <label for="usernameInput">Username:</label>
      <input type="text" class="form-control" id="usernameInput" name="username" placeholder="Enter a username"/>
    </div>
  </div>
  <div class="aRegisterInput">
    <div class="form-group">
      <label for="emailInput">Email:</label>
      <input type="email" class="form-control" id="emailInput" name="email" placeholder="Enter your email"/>
      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
  </div>
  <div class="aRegisterInput">
    <div class="form-group">
      <label for="pass1Input">Password:</label>
      <input type="password" class="form-control" id="pass1Input" name="password1" placeholder="Enter a password"/>
    </div>
  </div>
  <div class="aRegisterInput">
    <div class="form-group">
      <label for="pass2Input">Confirm your password:</label>
      <input type="password" class="form-control" id="pass2Input" name="password2" placeholder="Enter a password"/>
    </div>
  </div>
  <button class="btn btn-outline-success" type="submit" name="reg_user">Register your account</button>
</form>

<?php
  include("footer.php");
?>
