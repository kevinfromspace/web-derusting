<?php include("server.php"); ?>

<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php echo $title; ?></title>

  <!-- CSS -->
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <!-- jQuery UI -->
    <link href="css/jquery-ui.min.css" rel="stylesheet" />
    <!-- My own CSS -->
    <link href="css/style.min.css" rel="stylesheet"/>
</head>

<body>

  <div class="container-fluid">

    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
      <span class="navbar-brand mb-0 h1"><?php echo $title; ?></span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbar" aria-controls="myNavbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div id="myNavbar" class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php" id="homePageLink">Homepage</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="googlemapstest.php" id="googleMapsTestLink">Google Maps Test</a>
          </li>
        </ul>
      <?php if (!isset($_SESSION['username']) && !isset($_SESSION['success'])) : ?>
        <form method="post" class="form-inline my-2 my-md-0">
          <input class="navForm form-control mr-sm-2" type="text" name="username" placeholder="Your username..."/>
          <input class="navForm form-control mr-sm-2" type="password" name="password" placeholder="Your password..."/>
          <button id="loginButton" class="navForm navButtons btn btn-outline-success" type="submit" name="login_user">Login</button>
          <button id="registerButton" class="navForm navButtons btn btn-outline-light" type="button">Register</button>
        </form>
      <?php endif; if (isset($_SESSION['username']) || isset($_SESSION['success'])) : ?>
        <form method="post" class="form-inline my-2 my-md-0">
          <button id="logoutButton" class="btn btn-outline-danger" type="submit" name="logout_user">Logout</button>
        </form>
      <?php endif; ?>
      </div>
    </nav>

<?php

if (!isset($_SESSION['username'])) {
  echo '<p class="notice">Please login using the navigation bar above!</p>';
} else if (isset($_SESSION['username'])) {
  echo '<p class="loggedinText">Your username is ' . $_SESSION["username"] . ' and you are logged in!</p>';
}

?>
